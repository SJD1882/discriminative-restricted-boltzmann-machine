#!/usr/bin/python

"""Code for training the Discriminative Restricted Boltzmann Machine.

This script is to be run with the following command-line options with their
respective arguments:
    -d  Dataset list file (See examples/datasets).
    -m  Model configuration file (See examples/models).
    -o  Optimizer configuration file (See examples/optimizers).
"""


import getopt
import gzip
from IO import model_exists
from IO import save_model
import os
import cPickle
import sys

from collections import OrderedDict
from config import Config
from drbm import train_model
from utils import dict_product


def usage():
    """
    Print usage of script.
    """
    print ("Usage: \n\t$ train_models.py -m model.cfg -o optimizer.cfg "
           "-d dataset-list.txt\n")


def grid_search(mod_hypers, opt_hypers, data, root):
    """Search a grid of hyperparameters for the best model on given data. This
    can also be used to train a single model.

    Input
    -----
    mod_hypers: dict
      Model hyperparameters (as keys) and their values.
    opt_hypers: dict
      Optimization hyperparameters (as keys ) and their values.
    data: dict
      Contains inputs, labels and training/test fold information.
    root: string
      Path to folder in which to save the models.

    Output
    ------
    Each learned model, along with its hyperparameters and those of the
    optimizer and best validation scores, is saved to its own file in a folder
    named 'models' inside the folder specified by root.
    """
    # Carry out grid search
    mod_hyper_grid = dict_product(mod_hypers)
    for mod_hyper in mod_hyper_grid:
        opt_hyper_grid = dict_product(opt_hypers)
        for opt_hyper in opt_hyper_grid:
            if model_exists(mod_hyper, opt_hyper, root):
                # Don't re-learn if an identical model if it already exists
                print ("A saved file corresponding to the following (model and"
                       " optimization) hyperparameters exists. Skipping...")
                print mod_hyper
                print opt_hyper
            else:
                print ("Learning a model with the following (model and "
                       "optimization) hyperparameters")
                print mod_hyper
                print opt_hyper

                # Learn a model with one set of hyperparameters.
                grid_point = train_one_model(mod_hyper, opt_hyper, data)
                save_model(grid_point, root)

    print "Done with grid search.\n"


def train_one_model(mod_hypers, opt_hypers, data, init_params=None):
    """
    Evaluate one point in the grid.

    Input
    -----
    mod_hypers : dict
      Model hyperparameters (as keys) and their values.
    opt_hypers : dict
      Optimizer hyperparameters (as keys) and their values.
    data : dict
      A dictionary containing, inputs, labels and fold information.
    params : tuple(np.ndarray) [optional]
      Initial values of model parameters.

    Output
    -----
    grid_point : dict
      A dictionary containing learning hyperparameters, scores and model
      parameters.
    """
    # Data check
    n_folds = len(data['X']['train'])
    set_type = ['train', 'verify', 'test']

    # Data hyperparameters
    dat_hypers = {}
    dat_hypers['n_input'] = data['X']['train'][0].shape[1]
    dat_hypers['n_class'] = data['n_classes']

    params = []
    scores = []
    for fld in xrange(n_folds):
        print "\nFold %d" % (fld+1)

        # Prepare training and validation data for this fold
        x_train = data['X']['train'][fld]
        x_valid = data['X']['verify'][fld]
        y_train = data['y']['train'][fld]
        y_valid = data['y']['verify'][fld]
        if opt_hypers['eval_test'] == True:
            x_test = data['X']['test'][fld]
            y_test = data['y']['test'][fld]

        dat_hypers['n_train'] = len(data['X']['train'][fld])
        dat_hypers['n_valid'] = len(data['X']['verify'][fld]) 
        if opt_hypers['eval_test'] == True:
            dat_hypers['n_test'] = len(data['X']['test'][fld]) 
            dataset = ((x_train, y_train), (x_valid, y_valid), 
                       (x_test, y_test))
        else:
            dataset = ((x_train, y_train), (x_valid, y_valid))


        # NOTE: This if-else is needed because of the reference to individual
        # elements (folds) of the list init_params.
        if init_params is None:  # Learn model from scratch
            param, score = train_model(dataset=dataset, mod_hypers=mod_hypers,
                                       opt_hypers=opt_hypers,
                                       dat_hypers=dat_hypers)
        else:  # Update already learned model
            param, score = train_model(dataset=dataset, mod_hypers=mod_hypers,
                                       opt_hypers=opt_hypers,
                                       dat_hypers=dat_hypers,
                                       model_params=init_params[fld])

        params.append(param)
        scores.append(score)

    # Populate learning results dictionary
    grid_point = {}
    grid_point['mod_hypers'] = mod_hypers
    grid_point['opt_hypers'] = opt_hypers
    grid_point['models'] = params
    grid_point['validation'] = scores

    return grid_point


if __name__ == "__main__":
    try:
        CL_OPTARGS, _ = getopt.getopt(sys.argv[1:], 'd:hm:o:', ['help'])
    except getopt.GetoptError as err:
        print str(err)
        usage()
        sys.exit(2)

    # Parse command-line options and arguments
    for o, a in CL_OPTARGS:
        if o == '-d': #  Dataset list file
            assert os.path.isfile(a), ("specified dataset file %s doesn't "
                                       "exist" % (a))
            dataset_file = a
            f_dataset = open(dataset_file, 'r')
        elif o == '-m': #  Model configuration file
            assert os.path.isfile(a), ("specified config file %s doesn't "
                                       "exist" % (a))
            config_file = a
            f_config = file(config_file)
            mod_cfg = OrderedDict(Config(f_config)) # Duck-typing
        elif o == '-o': #  Optimizer configuration file
            assert os.path.isfile(a), ("specified config file %s doesn't "
                                       "exist" % (a))
            config_file = a
            f_config = file(config_file)
            opt_cfg = OrderedDict(Config(f_config)) # Duck-typing
        elif o in ('-h', '--help'): # Help!
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"

    # Iterate over the various datasets
    for dataset in f_dataset.readlines():
        dataset = dataset.strip()
        out_path = os.path.dirname(dataset)

        # Load data
        data = cPickle.load(gzip.open(dataset, 'rb'))
        data = dict((key, data[key]) for key in ('X', 'y', 'n_classes'))
        print "Dataset: %s\n" % (out_path)

        # Learn and save model(s), i.e, save the best model and optimization
        # hyperparameters, and their respective validation scores.
        grid_search(mod_cfg, opt_cfg, data, os.path.join(out_path, 'models'))

    f_dataset.close()
    print "End of program."
