#!/usr/bin/python

"""Code for evaluating previously learned connectionist models for melody.

This script is to be run with the following command-line options with their
respective arguments:
    -e  Evaluation configuration file name.
    -d  Dataset list file name.

The EVALUATION CONFIGURATION FILE is parsed using the config Python module, and
must contain the following hyperparameter:
    method       : Whether the evaluation is to be 'online' or 'offline'.

... and if the evaluation method is 'online', the following hyperparameters
will also have to be specified:
    opt_type             : Optimizers type - stoch-gd or batch-gd
    learning_rate        : Learning rate.
    schedule             : The type of learning 
    rate_param           : The decay parameter to use in a learning rate
                           schedule.
    max_epoch            : Maximum number of training epochs.

and optionally, the following hyperparameters (not implemented yet):
    initial_momentum     : Momentum at the start of training.
    final_momentum       : Momentum to switch to during training.
    momentum_switchover  : Number of epochs after which to switch from initial
                           to final momentum.

The DATASET LIST FILE points to a set of files, each of which contains some
data to train the models. The data is contained in a dictionary saved in a
cPickle file, which has the following fields:
    X              : Input sequences (either a list or a Numpy ndarray)
    y              : Target sequences (either a list or a Numpy ndarray)
    fold_labels    : Fold labels for all the data-points (either a list or a
                     Numpy ndarray)
    n_classes      : Number of prediction classes
    softmax_config : Pattern of softmax groups (only applicable to the RBM and
                     the RTRBM)
    n_maps         : Essentially the context length (only applicable to the
                     NPMM, which hasn't yet been implemented)
"""


import getopt
import gzip
import numpy as np
import os
import cPickle
import sys

from collections import OrderedDict
from config import Config
from drbm import test_model
from IO import load_model
from IO import update_model
from utils import dict_product


def usage():
    """
    Print usage of script.
    """
    print ("Usage: \n\t$ test_models.py -d dataset-list.txt "
           "-e evaluation-config.cfg\n") 


def evaluate_models(eva_hypers, data, root):
    """Evaluate all the models saved in a folder on given data.

    Input
    -----
    data : dict
      Contains inputs, labels and training/test fold information.
    root : string
      Path to folder which contains the models.
    eva_hypers : dict
      Dictionary containing evaluation hyperparameters.

    Output
    ------
    Each model in the folder root is evaluated, and the previously saved model
    file is updated with the results.
    """

    # Iterate over all models learned for the dataset specified by root
    for fn in os.listdir(root):
        if fn.startswith('model') and fn.endswith('.pkl.gz') and not 'eval' in fn:
            print "Evaluating model saved in:\n\t%s\n" % (fn)

            # Generate and iterate over evaluation hyperparameter grid
            eva_hyper_grid = dict_product(eva_hypers)
            for eva_hyper in eva_hyper_grid:
                model = load_model(os.path.join(root, fn))
                # XXX: Each of the below returned variables will be a 2-tuple
                # in case of online and semi-online evaluation as they'll also 
                # include validation scores, predictions and probabilities.
                predictions, probabilities, scores = \
                    evaluate_one_model(model, data, eva_hyper) 
                model['eva_hypers'] = eva_hyper

                if eva_hyper['method'] == 'offline':
                    model['probabilities'] = [probability for probability in
                                              probabilities]
                    model['predictions'] = [prediction for prediction in
                                            predictions]
                    model['test_offline'] = scores
                elif eva_hyper['method'] == 'online':
                    # Replace offline validation scores by the online ones
                    model['probabilities'] = [probability[1] for probability in
                                              probabilities]
                    model['predictions'] = [prediction[1] for prediction in
                                            predictions]
                    model['validation_offline'] = model['validation']
                    model['validation'] = [score[0][0] for score in scores]
                    model['test_online'] = [(score[0][1], score[1][1]) 
                                            for score in scores]
                elif eva_hyper['method'] == 'semi-online':
                    # Replace offline validation scores by the semi-online ones
                    model['probabilities'] = [probability[1] for probability in
                                              probabilities]
                    model['predictions'] = [prediction[1] for prediction in
                                            predictions]
                    model['validation_offline'] = model['validation']
                    model['validation'] = [score[0][0] for score in scores]
                    model['test_semionline'] = [(score[0][1], score[1][1]) 
                                                for score in scores]
                else:
                    assert False, 'unknown evaluation type'


                model = update_model(model, os.path.join(root, fn))

    print "Done with evaluation."


def evaluate_one_model(model, data, eva_hypers): 
    """
    Evaluate one point in the grid search.

    Input
    -----
    model : dict
      A dictionary containing model (and its optimization) hyperparameters,
      parameters, validation scores.
    data : dict
      Contains inputs, labels and training/test fold information.
    eva_hypers : dict
      Dictionary containing evaluation hyperparameters.

    Output
    -----
    scores : tuple
      A tuple containing model predictions, prediction probabilities and the
      corresponding scores.
    """
    # Data check
    n_folds = len(data['X']['train'])
    set_type = ['train', 'verify', 'test']
    
    # Data hyperparameters
    dat_hypers = {} # Note that I don't need the variable "data" anymore
    dat_hypers['n_input'] = data['X']['train'][0].shape[1]
    dat_hypers['n_class'] = data['n_classes']

    scores = []
    probabilities = []
    predictions = []
    for fld in xrange(n_folds):
        print "\nFold %d" % (fld+1)
        
        # Prepare test data for this fold
        x_test = data['X']['test'][fld]
        y_test = data['y']['test'][fld]

        test_data = (x_test, y_test)
        dat_hypers['n_test'] = x_test.shape[0]

        # Evaluate model
        y_prob, cr_ent, y_pred, acc = test_model(
            (model['models'][fld], model['mod_hypers']), 
            (test_data, dat_hypers), eva_hypers)
        predictions.append(y_pred)
        probabilities.append(y_prob)
        scores.append((cr_ent, acc))

    return predictions, probabilities, scores


if __name__ == "__main__":
    try:  # Parse command-line arguments using getopt
        cl_optargs, _ = getopt.getopt(sys.argv[1:], 'd:e:h', ['help'])
    except getopt.GetoptError as err:
        print str(err)
        usage()
        sys.exit(2)

    for o, a in cl_optargs:
        if o == '-d':  # Dataset list file
            assert os.path.isfile(a), "specified dataset file doesn't exist"
            dataset_file = a
            f_dataset = open(dataset_file, 'r')
        elif o == '-e':  # Evaluation configuration file
            assert os.path.isfile(a), "specified config file doesn't exist"
            config_file = a
            f_config = file(config_file)
            eva_cfg = OrderedDict(Config(f_config))  # Duck-typing
        elif o in ('-h', '--help'):  # Help!
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"

    # Iterate over the various datasets
    for dataset in f_dataset.readlines():
        dataset = dataset.strip()
        out_path = os.path.dirname(dataset)

        # Load data
        data = cPickle.load(gzip.open(dataset, 'rb'))
        data = dict((key, data[key]) for key in ('X', 'y', 'n_classes'))
        print "Dataset: %s\n" % (out_path)

        # Evaluate model(s) learned and saved on this dataset
        root_dir = os.path.join(out_path, 'models')
        if os.path.isdir(root_dir):
            evaluate_models(eva_cfg, data, root_dir)
        else:
            print "The directory %s doesn't exist. Skipping..." % (root_dir)

    f_dataset.close()
    print "End of program."
